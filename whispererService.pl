#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use POSIX qw(strftime);
use JSON; #sudo apt-get install libjson-perl

my $lockPath = "/var/lock/whispererService.lock";

sub getInstances($$$);
sub createWhisperer($$$$);
sub uniq($);
sub processTask($);
sub read_file($);

#Stream output
$| = 1;

#Colored output
my $COLOR_ERROR = "\033[31m";
my $COLOR_SUCCESS = "\033[32m";
my $COLOR_RESET = "\033[39;49m";
my $COLOR_INFO = "\033[33m";
my $ok = $COLOR_SUCCESS . "Ok\n" . $COLOR_RESET;
my $SLEEP_TIME = 2;

#Should be called with a config file
my $num_args = $#ARGV + 1;
if ($num_args != 1) {
    print "Usage: whispererService.pl config.json\n";
    exit;
}

# config file
my $configFile = $ARGV[0];

print "Putting lock file $lockPath... ";
if(-e $lockPath){
    my $pid = read_file($lockPath);
    my $exists = kill 0, $pid;
    if($exists){
        print $COLOR_ERROR, "\n   -> Error: Service is already running as process $pid.\n", $COLOR_RESET;
        exit(1);
    }
    else {
        print "\n   -> Service did not die correctly. Removing lock file... But ", $COLOR_ERROR, "check logs!\n", $COLOR_RESET;
        unlink $lockPath;
    }
}

#Put lock file
open(my $fh, '>', $lockPath);
print $fh $$;
close $fh;
print $ok;

#Remove lock on exit
sub onEnd {
    print "\n---------------------------------------------------------------------------\n";
    print "End of service required. Removing lock file... ";
    unlink $lockPath;
    print $ok, "Exiting.\n";
    exit 0;
};
$SIG{INT} = \&onEnd; # ctrl + c
$SIG{TERM} = \&onEnd; # standard kill

while(1){
    print "\n---------------------------------------------------------------------------\n";
    print "Running at: " . strftime "%Y-%m-%d %H:%M:%S", localtime;

    print "Opening config... ";
    my $config = eval { read_file($configFile) };
    if($@) {
        print $COLOR_ERROR, "\n   -> Error: Could not read config file.\n", $COLOR_RESET;
        print "      $@";
        exit 1;
    }
    print $ok;

    print "Parsing config... ";
    $config = eval { decode_json $config };
    if($@) {
        print $COLOR_ERROR, "\n   -> Error: Could not parse config.\n", $COLOR_RESET;
        print "      $@";
        exit 1;
    }
    print $ok;

    foreach my $task (@{$config->{dockerTasks}}) {
        eval {
            processTask($task);
        };
        if($@) {
            my $error = $COLOR_ERROR . "Error in last task processing: $@. Ignoring.\n" . $COLOR_RESET;
            print $error;
            warn $error;
        }
    }
    print "\nAll configs done, waiting for 5 min for new check\n";
    #Sleep (in s) before next trial
    sleep 60*$SLEEP_TIME;
}

# Get instances for this task in the cluster
# Check if a whisperer is active on them
# Launch missing whisperers
sub processTask($){
    my $task = shift;
    my $version = defined $task->{version} ? ':' . $task->{version} : ':latest';
    my @configs;
    if($task->{whispererMesh}){
        push @configs, $task->{whispererMesh};
    }
    elsif($task->{whisperers}){
        @configs = @{$task->{whisperers}};
    }
    else{
        print $COLOR_ERROR , "\n  No configuration defined\n", $COLOR_RESET;
    }

    print "\nTask to monitor: " . $COLOR_INFO . (defined $task->{master} ? $task->{master} : "localhost") . " - $task->{name}\n" . $COLOR_RESET;

    #Fetch instances

    # Find nodes where gateway task are running
    #    docker service ps itproduction_gateway --filter "desired-state=running" --format "{{.ID}}\t{{.Name}}\t{{.Node}}"
    # Output:
    #    kpd3o70n6szi	itproduction_gateway.kj0afd0qipebgy5lupasiailm	node-3.streetsmart.development
    #    e8p4cfm1uvrq	itproduction_gateway.rtzfjr9bl5gonnhmp1sk7ztcz	node-1.streetsmart.development
    #    0v5ts70fvpvt	itproduction_gateway.vysdun6fw63gdsw19j7o4qtz6	node-2.streetsmart.development

    my @instances;
    my @nodes;
    if(defined $task->{master}){
        my $findNodesOutput = `docker -H $task->{master} \\\
                                  service ps $task->{name} \\\
                                  --filter 'desired-state=running' \\\
                                  --format '{{.ID}}\t{{.Name}}\t{{.Node}}'`;

        @instances = $findNodesOutput =~ /^.+\t.*\t(.*)$/mg;

        print "  Found " . scalar(@instances) . " instance(s) on ";
        @nodes = uniq(\@instances);
        print scalar(@nodes) . " node(s)\n";
    }
    else{
        my $findNodesOutput = `docker \\\
                                  container ls \\\
                                  --filter 'name=$task->{name}' \\\
                                  --format '{{.ID}}\t{{.Names}}'`;

        @instances = $findNodesOutput =~ /^.+\t(.*)$/mg;

        print "  Found " . scalar(@instances) . " instance(s) on localhost\n";
        @nodes = ("localhost");
    }

    if(scalar(@instances) > 0){
        my %containers;

        #List all instances with whisperers
        foreach my $node (@nodes) {
            $containers{$node} = getInstances($node, $task->{name}, \@configs);
        }

        #Output current status
        my $newWhispererToLaunch = 0;
        foreach my $node (@nodes) {
            foreach my $container (@{$containers{$node}}) {
                print "    Node $container->{node} - Container $container->{name} ($container->{id})\n";
                if(defined $container->{whisperer}){
                    print "      -> Monitored by $container->{whisperer} ($container->{whispererConfig})\n";
                }
                if(not defined $container->{whisperer}){
                    $newWhispererToLaunch++;
                }
            }
        }

        if($newWhispererToLaunch > 0){

            if($task->{whispererMesh}){
                print "\n  Unlimited instances with same conf\n";

                my $whispMeshConf = $task->{whispererMesh};
                foreach my $node (@nodes) {
                    foreach my $container (@{$containers{$node}}) {
                        if(not defined $container->{whisperer}){
                            createWhisperer($task->{master}, $container, $whispMeshConf, $version);
                        }
                    }
                }
            }
            elsif($task->{whisperers}){
                my @whispConfFiles = @{$task->{whisperers}};
                print "\n  Limited configurations defined\n";

                my $whispConfigItem = 0;
                foreach my $node (@nodes) {
                    foreach my $container (@{$containers{$node}}) {
                        if(defined $container->{whispererConfig}){
                            my @temp;
                            foreach my $config (@whispConfFiles){
                                if($config ne $container->{whispererConfig}){
                                    push @temp, $config;
                                }
                            }
                            @whispConfFiles = @temp;
                        }
                    }
                }

                my $availableConfigs = scalar @whispConfFiles;
                print "\n  $availableConfigs unused whisperer configuration:\n";
                foreach my $config (@whispConfFiles){
                    print "    $config";
                }

                #Create new whisp
                if(scalar @whispConfFiles > 0){
                    print "\n  Creating $newWhispererToLaunch Whisperers:\n";
                    foreach my $node (@nodes) {
                        foreach my $container (@{$containers{$node}}) {
                            if(not defined $container->{whisperer} and $whispConfigItem < $availableConfigs){
                                createWhisperer($task->{master}, $container, $whispConfFiles[$whispConfigItem], $version);
                                $whispConfigItem++;
                            }
                        }
                    }
                }
                else{
                    print $COLOR_ERROR , "\n  Not enough whisperers to monitor all instances\n", $COLOR_RESET;
                }
            }
            else{
                print $COLOR_ERROR , "\n  No configuration defined\n", $COLOR_RESET;
            }
        }
        else{
            print $COLOR_SUCCESS , "\n  No new Whisperer to launch.\n", $COLOR_RESET;
        }
    }
    else{
        print $COLOR_ERROR, "\n  No instance on " . (defined $task->{master} ? $task->{master} : "localhost") . "\n", $COLOR_RESET;
    }
}

# Get list of task instances on a node and get associated whisperers
# Input:
#  - $node (string) : FQDN of node
#  - $task (string) : Name of task to monitor
# Output:
#  - \@containers (pointer to array)
#    [{
#      node: node of cluster
#      name: name of container
#      id: id of container
#      whisperer: id of whisperer container monitoring to task (if exists)
#      whispererConfig: original configuration for this whisperer
#     },{}]
sub getInstances($$$) {
    my $node = shift;
    my $task = shift;
    my $configs = shift;
    my $target = $node ne "localhost" ? "-H $node" : "";

    # Find container id for those task on the node
    #    docker -H node-2.streetsmart.development ps --filter "status=running" --filter "is-task=true" --filter "name=itproduction_gateway" --format="{{.ID}}\t{{.Names}}"
    # Output:
    #    82120bbbc084	itproduction_gateway.yqq659msi7u07oamj5xf47y55.98t3w87d0esildcbpgtnn0tpn

    my $containers = `docker $target ps \\\
                              --filter 'status=running' \\\
                              --filter 'is-task=true' \\\
                              --filter 'name=$task' \\\
                              --format='{{.ID}}\t{{.Names}}'`;

    my @containers;
    while ($containers =~ /^(.*)\t(.*)$/mg) {
        my %container;
        $container{id} = $1;
        $container{name} = $2;
        $container{node} = $node;
        push @containers, \%container;
    }

    my $whisps = `docker $target ps \\\
                          --filter 'status=running' \\\
                          --filter 'name=spider' \\\
                          --format='{{.ID}}\t{{.Names}}'`;

    while ($whisps =~ /^(.*)\t(?:spider-.*?-)?config-(.*)$/mg) {
        # Get network monitored by whisperer
        #    docker inspect spider --format="{{.HostConfig.NetworkMode}}"
        # Output:
        #    container:82120bbbc084e94399eba277fd3f04f1710ec40a71116d9210cb3d4b05382992

        my $whispId = $1;
        my $configuration = $2;

        # if this spider is from the configuration of ongoing task
        # we may have same container with more than one whisperer on it
        if (grep( /^$configuration$/, @{$configs})){
            my $network = `docker $target inspect $whispId \\\
                                --format='{{.HostConfig.NetworkMode}}'`;

            my ($container) = $network =~ /container:(.{12})/;
            # container may no be defined if the container on which the whisperer was attached died
            if(defined $container){
                my $found = 0;
                foreach my $cont (@containers) {
                    if ($cont->{id} eq $container) {
                        $cont->{whisperer} = $whispId;
                        $cont->{whispererConfig} = $configuration;
                        $found = 1;
                    }
                }

                #container monitored may have died, in this case, remove old whisperer
                unless($found == 1){
                    my $running = `docker $target inspect $container \\\
                                --format='{{.State.Running}}'`;
                    if($running !~/true/){
                        print $COLOR_ERROR , "    Found Whisperer $whispId ($configuration) whose attached host is stopped. Stopping it.\n", $COLOR_RESET;
                        `docker $target rm -f $whispId`;
                    }
                }
            }
        }
    }

    return \@containers;
}
#TODO: Get image version sha in repo and compare with running to see if need to kill and restart

# Create a whisperer container monitoring the given container with given config
# Input:
#  - \%container (pointer to hash) : container object given by getInstances
#  - $whispConfFile : filename of configuration
# Output:
#  - N/A
sub createWhisperer($$$$) {
    my $master = shift;
    my $container = shift;
    my $whispConfFile = shift;
    my $version = shift;

    my $node = $container->{node};
    my $target = $node ne "localhost" ? "-H $node" : "";

    print "    Launching Whisperer for $container->{name} ($container->{id})\n";
    print "      Loading conf $whispConfFile\n";
    my $whispConf = read_file($whispConfFile);

    my $services;
    if(defined $master || ((`docker info --format '{{.Swarm.LocalNodeState}}'` eq "active\n") and (`docker info --format '{{.Swarm.ControlAvailable}}'` eq "true\n"))){
        print "      Getting list of services\n";
        my $masterTarget = defined $master ? "-H $master" : "";
        $services = `docker $masterTarget \\\
                            service ls --format="{{.Name}}"`;
        print "        " . scalar(split('\n',$services)). " services found\n";
    }
    else{
        print "      Localhost is a worker, cannot preload services\n";
    }

    print $COLOR_INFO, "      Launching Whisperer on $container->{node}\n        ", $COLOR_RESET;

    my $resolveServices = defined $services ? "-e 'HOSTS_TO_RESOLVE=$services'" : '';
    print `docker $target \\\
                  run \\\
                  -d \\\
                  -e 'CONFIG=$whispConf' \\\
                  -e 'PARENT_HOSTNAME=$container->{node}' \\\
                  -e 'CONTAINER_NAME=$container->{name}' \\\
                  $resolveServices \\\
                  --network container:$container->{id} \\\
                  --restart=no \\\
                  --name='spider-$container->{id}-config-$whispConfFile' \\\
                  --rm \\\
                  registry.gitlab.com/spider-analyzer/public-images/whisperer$version`;
}

# Given an array, return
# Input:
#  - \@array (pointer to array) : array of string
# Output:
#  - @array : new array with unique sorted values of the input array
sub uniq($){
    my $array = shift;
    my @array = sort @{$array};

    my @single;
    for (my $i=0; $i < scalar(@array); $i++) {
        if( $i == 0 ){
            push @single, $array[$i];
        }
        elsif( $i > 0 and $array[$i] ne $array[$i-1]){
            push @single, $array[$i];
        }
    }
    return @single;
}

sub read_file($){
    return do{local(@ARGV,$/)=shift;<>};
}
